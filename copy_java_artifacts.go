package main

import (
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"golang.org/x/net/context"
	"strings"
	"log"
	"os/exec"
)

func main() {
	cli, err := client.NewEnvClient()
	if err != nil {
		panic(err)
	}

	containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{})
	if err != nil {
		panic(err)
	}

	for _, container := range containers {
		fmt.Println("Container Id " + container.ID)
		fmt.Println("Container CMD  " + container.Command)
		s := container.Command
		if (strings.Contains(s, "java") || strings.Contains(s, "JAVA")) {
			split := strings.Split(s, " ")
			for _, str := range split {
				if strings.HasSuffix(str, ".jar") || strings.HasSuffix(str, ".war") {
					copyFilefromContainerToHost(str, container)
				}
			}

		}
	}
}

func copyFilefromContainerToHost(filename string, container types.Container) {
	fmt.Println(filename)
	destDir := "."
	cmd := exec.Command("docker", "cp", container.ID+":"+filename, destDir+filename)
	log.Printf("Running command and waiting for it to finish...")
	err := cmd.Run()
	log.Printf("Command finished with error: %v", err)
}
